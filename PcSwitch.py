import logging
import os
import time
import subprocess

from flask import Flask
from flask_ask import Ask, request, session, question, statement
import RPi.GPIO as GPIO


#
# your url is:

app = Flask(__name__)
ask = Ask(app, "/")
logging.getLogger('flask_ask').setLevel(logging.DEBUG)

STATUSON = ["on", "anzuschalten", "anzumachen", "anmachen", "anschalten",
            "an"]  # alle Werte, die als Synonyme im Slot Type definiert wurden
STATUSOFF = ["off", "auszuschalten", "auszumachen", "ausmachen", "ausschalten", "aus"]


# setup GPIO pins
SWITCH_PIN = 23
READ_PC_PIN = 24

last_switch = 0


def init():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(SWITCH_PIN, GPIO.OUT)
    GPIO.setup(READ_PC_PIN, GPIO.IN)

    # start localtunnel and ensure we got the right subdomain
    correct_subdomain = False
    while not correct_subdomain:
        cmd = 'lt --port 5000 --subdomain raspberry-pc-switch-1337'
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        stdout = []

        while p.poll() is None:
            line = p.stdout.readline().decode("utf-8")
            print(line)
            if 'your url is:' in line:
                if 'https://raspberry-pc-switch-1337.loca.lt' in line:
                    correct_subdomain = True
                else:
                    p.terminate()
                    print('Localtunnel: ' + line)
                    print('Could not aquire the correct localtunnel subdomain, trying again in 60sec ...')
                    time.sleep(60)
                    print('--------------------------------------')
                break


@ask.launch
def launch():
    speech_text = 'Wilkommen zur Raspberry Pi Automatisierung.'
    return question(speech_text).reprompt(speech_text).simple_card(speech_text)


@ask.intent('SwitchIntent', mapping={'status': 'status'})
def switch_intent(status, room):
    global last_switch
    if time.time() < last_switch + 60:
        return statement('Sorry, der Befehl ist gerade auf cooldown.')

    pc_state = GPIO.input(READ_PC_PIN)
    print('Pc state: ' + str(pc_state))
    print('Alexa post: ' + str(status))
    if status in STATUSON and not pc_state:
        last_switch = time.time()
        GPIO.output(SWITCH_PIN, GPIO.HIGH)
        time.sleep(0.2)
        GPIO.output(SWITCH_PIN, GPIO.LOW)
        time.sleep(0.2)
        return statement('Computer wurde angeschaltet')
    elif status in STATUSOFF and pc_state:
        last_switch = time.time()
        GPIO.output(SWITCH_PIN, GPIO.HIGH)
        time.sleep(0.2)
        GPIO.output(SWITCH_PIN, GPIO.LOW)
        time.sleep(0.2)
        return statement('Computer wurde ausgeschaltet')
    else:
        return statement('Sorry, der Befehl ist leider nicht möglich.')


@ask.intent('AMAZON.HelpIntent')
def help():
    speech_text = 'You can say hello to me!'
    return question(speech_text).reprompt(speech_text).simple_card('HelloWorld', speech_text)


@ask.session_ended
def session_ended():
    return "{}", 200


if __name__ == '__main__':
    if 'ASK_VERIFY_REQUESTS' in os.environ:
        verify = str(os.environ.get('ASK_VERIFY_REQUESTS', '')).lower()
        if verify == 'false':
            app.config['ASK_VERIFY_REQUESTS'] = False
    init()
    app.run(debug=True)
